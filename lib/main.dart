import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
String NameOfPet ='';
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'AlertDialog',
      home: Scaffold(
          appBar: AppBar(
            title: Text('AlertDialog'),
          ),
        body: MyAlert(),//
      ),
    );
  }
}
class MyAlert extends StatelessWidget{
  @override
  Widget build(BuildContext context)
  {
   return Center(
    child: RaisedButton(
        child: Text('Show Alert'),
    onPressed: () {
  showAlertDialog(context);
        }
    )
   );
  }
}

showAlertDialog(BuildContext context)
{
  //Saves the user input
  return showDialog(
  context: context,
    barrierDismissible: true, //Allows the user to dismiss the dialog by pressing outside the popup
    builder: (BuildContext context)
    {
      return AlertDialog(
        title: Text('Enter the name of your pet'),
        content: new Row(
          children: [
            new Expanded( //Children within expanded expand inside of empty space
              child: new TextField(
                autofocus: true,
                decoration: new InputDecoration(
                  labelText: 'Pet Name', hintText: "ex. Fluffy"
                ),
                onChanged: (value){
                  NameOfPet= value;
                },
              ),

            )

          ],

        ),
        actions: [
          FlatButton(
            child: Text('Ok'),
            onPressed: (){
              ShowResultDialog(context);

              }
          )
        ],
      );

    }
  );
}

ShowResultDialog(BuildContext context)
{
  return showDialog(
    context: context,
    barrierDismissible: true,
      builder: (BuildContext context){
      return AlertDialog(
        title: Text("The name of your pet is ${NameOfPet}" ),
        actions: [
          FlatButton(
            child: Text("Ok??"),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )

        ],


      );


      }

  );


}
